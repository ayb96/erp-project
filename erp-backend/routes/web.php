<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;

Route::get('/', function () {
    
});

Route::get('/addpost', [PostController::class,'addPost']);
Route::get('/addcomment/{id}', [PostController::class,'addComment']);
Route::get('/getcomments/{id}', [PostController::class,'getCommnetsByPost']);
