<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\EmployeeeController;
use App\Http\Controllers\KpiController;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Employees Routes

Route::get('allemployee', [EmployeeeController::class, 'searchingz']);
Route::get('search/{key}', [EmployeeeController::class, 'search']);
Route::post('addemployee', [EmployeeeController::class, 'addEmployee']);
Route::delete('delete-employee/{id}', [EmployeeeController::class, 'deleteEmployee']);
Route::put('edit-employee/{id}', [EmployeeeController::class, 'editEmployee']);
Route::get('employeeteam/{id}', [EmployeeeController::class, 'employeeteam']);
Route::get('employeeproject/{id}', [EmployeeeController::class, 'employeeproject']);


Route::get('employeez', [EmployeeeController::class, 'GetAllEmployee']);
Route::get('edit-employee/{id}', [EmployeeeController::class, 'editEmployeez']);





//Teams Routes
Route::post('addteam', [TeamController::class, 'addTeam']);
Route::put('edit-team/{id}', [TeamController::class, 'editTeam']);
Route::get('teams', [TeamController::class, 'getTeams']);
Route::get('team', [TeamController::class, 'getTeam']);
Route::delete('delete-team/{id}', [TeamController::class, 'deleteTeam']);
Route::get('teamprojectsss', [TeamController::class, 'getProjectTeam']);

//Projects Routes
Route::put('edit-project/{id}', [ProjectController::class, 'editProject']);
Route::post('addproject', [ProjectController::class, 'addProject']);
Route::get('projects', [ProjectController::class, 'getProjects']);
Route::get('project', [ProjectController::class, 'getProject']);
Route::delete('delete-project/{id}', [ProjectController::class, 'deleteProject']);
Route::get('searchproduct/{key}', [ProjectController::class, 'searchproject']);

Route::post('logout', [UserController::class, 'logout']);
Route::post('login', [UserController::class, 'login']);


//Kpis Routes
Route::post('addkpi', [KpiController::class, 'addkpi']);
Route::get('allkpi', [KpiController::class, 'allkpi']);
Route::get('paginationkpi', [KpiController::class, 'paginationkpi']);
Route::get('searchkpi/{key}', [KpiController::class, 'searchkpi']);


//Protected route
Route::group(['middleware'=>['auth:sanctum','isAPIAdmin']], function () {

    Route::post('employee', [EmployeeController::class, 'createEmployee']);
//    Route::delete('delete/{id}', [ProductController::class, 'delete']);
    Route::put('employee/{id}', [EmployeeController::class, 'updateEmployee']);
    Route::delete('deleteemployee/{id}', [EmployeeController::class, 'deleteEmployee']);
    Route::post('logout', [UserController::class, 'logout']);
    Route::post('authregister', [UserController::class, 'authregister']);
    Route::get('/checkingAuthenticated', function(){
        return response()->json([
            'message'=>'you are in', 'status'=>200
        ], 200);
    });
});

