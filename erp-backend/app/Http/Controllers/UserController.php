<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{


    function authregister(Request $req){
        $fields = Validator::make($req->all(),[
            'name'=>'required|string',
            'email'=>'required|string|unique:users,email',
            'password'=>'required|string|min:3'

        ]);

        if($fields->fails()){
            return response()->json([
                'errors' => $fields->errors(),
                'status'=>422,
            ]);
        }

        $user= new User;
        $user->name = $req->input('name');
        $user->email = $req->input('email');
        $user->password = Hash::make($req->input('password'));
        $user->save();

        $token = $user->createToken($user->email.'_Token')->plainTextToken;


        return response()->json([
            'status'=>200,
            'username'=>$user->name,
            'token'=>$token,
            'message'=>'Registered Successfully'

        ]);
    }


    public function logout(Request $req)
    {
        auth()->user()->tokens()->delete();
        return response()->json([
            'status'=>200,

            'message'=> 'logged out'
        ]);
    }







    public function login(Request $req){
        $fields=$req->validate([

            'email'=>'required|string',
            'password'=>'required|string'
        ]);
        $user= User::where('email',$fields['email'])->first();
        if(!$user || !Hash::check($fields['password'], $user->password)){
            return response()->json([
                'status'=>401,
                'message'=>'Email or password is not matched'
            ]);
        }else{
            if($user->role == '1')
            {
                $token = $user->createToken('token', ['server:admin'])->plainTextToken;
            }
            else
            {
                $token = $user->createToken('token', [''])->plainTextToken;
            }
        }
        return response()->json([
            'status'=>200,
            'username'=>$user->name,
            'token'=>$token,
            'message'=>'Registered Successfully'
        ]);
    }
}
