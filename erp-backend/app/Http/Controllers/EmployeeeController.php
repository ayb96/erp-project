<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Employeee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeeController extends Controller
{
    //pagiatation part
    function searchingz()
    {
        return Employeee::with('teams')->paginate(3);
    }
    function search($key)
    {
        return Employeee::with('teams')->where('first_name','Like', "%$key%")->paginate(1);
    }

    //get all employee for the dropdown
     public function GetAllEmployee(){
        return Employeee::all();
     }





    function addEmployee(Request $request){
        $validator =Validator::make($request->all(),[
            'firstname'=>'required|string|min:3',
            'lastname'=>'required|string',
            'email'=>'required|email|unique:employees,email',
            'phonenumber'=>'required|string',
            'file'=>'required',
            'team'=>'required',
            'project'=>'required',

        ]);
        if($validator->fails()){
            return response()->json([
                'status'=>422,
                'errors' => $validator->errors(),
            ]);
        }
        $employee = new Employeee;
        $employee->first_name= $request->input('firstname');
        $employee->last_name= $request->input('lastname');
        $employee->email= $request->input('email');
        $employee->phone_number= $request->input('phonenumber');
        $employee->file_path= $request->file('file')->store('products');
        $employee->team_id= $request->input('team');
        $employee->project_id= $request->input('project');
        $employee->save();
        return response()->json([
            'status'=>200,
            'message'=>'Employee edited Successfully'
        ]);
    }


    function deleteEmployee($id){
        $employee = Employeee::find($id);
        if($employee){
            $employee->delete();
            return response()->json([
                'status'=>200,
                'message'=>'Employee deleted Successfully'
            ]);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>'Employee Not Found'
            ]);
        }
    }


    function editEmployee(Request $request,$id){

        $validator  = Validator::make($request->all(),[
            'firstname'=>'required|string',
            'lastname'=>'required|string',
            'email'=>'required',
            'phonenumber'=>'required|string',
            'file'=>'required',
            'team'=>'required',
            'project'=>'required',
        ]);

//        if($validator->fails()){
//            return response()->json([
//                'status' => '422',
//                'errors' => $validator->errors(),
//
//            ]);
//        }

        $employee = Employeee::find($id);
        dump($request->file('file'));
        if($employee){
            $employee->first_name= $request->input('firstname');
            $employee->last_name= $request->input('lastname');
            $employee->email= $request->input('email');
            $employee->phone_number= $request->input('phonenumber');
        //    $employee->file_path= $request->input('file');
            $employee->file_path= $request->file('file')->store('products');
//            if($request->hasfile('file')){
//                $file = $request->file('file');
//                $extension = $file->getClientOriginalExtension();
//                $filename = time() . '.' . $extension;
//                $file->move('products', $filename);
//                $employee->file_path = $filename;
//            }
            $employee->team_id= $request->input('team');
            $employee->project_id= $request->input('project');
            dump($employee);
            $employee->save();
            return response()->json([
                'status'=>200,
                'message'=>'Employee edited Successfully'
            ]);
        }else{
            return response()->json([
                'status' => '404',
                'errors' => 'employee not found',
            ]);
        }


    }










    //testing and might be used

    function employeeteam($id)
    {
        return Employee::where('team_id',$id)->get();
    }

    function employeeproject($id)
    {
        return Employee::where('project_id',$id)->get();
    }




}
