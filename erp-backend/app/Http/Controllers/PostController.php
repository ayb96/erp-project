<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
class PostController extends Controller
{
    public function addPost()
    {
        $post = new Post();
        $post->title='Second Post Tilte';
        $post->body='Second Post Description';
        $post->save();
        return 'post has been created';
    }
    public function addComment($id)
    {
        $post = Post::find($id);
        $comment = new Comment();
        $comment->comment='rahhal Comment';
       
        $post->comments()->save($comment);
        return 'comment has been posted for post id 2';
    }

    public function getCommnetsByPost($id)
    {
        $comments = Post::find($id)->comments;
        return $comments;
    }


}
