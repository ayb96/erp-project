<?php

namespace App\Http\Controllers;

use App\Models\Kpi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class KpiController extends Controller
{
    public function paginationkpi(){
        return Kpi::with('employee')->paginate(3);
    }
    public function searchkpi($key){
        return Kpi::where('name', 'like', "%$key%")->paginate(1);
    }
    public function allkpi(){
        return Kpi::all();
    }


    public function addkpi(Request $request)
    {
        $validate = Validator::make($request->all(),[
            "name"=>"required|unique:kpis",
            "value"=>"required",
            "employee"=>"required"
        ]);
        if($validate->fails()){
            return response()->json([
               'errors' => $validate->errors(),
                'status'=>422,
            ]);
        }
        $kpi = new Kpi;
        $kpi->name = $request->input('name');
        $kpi->value= $request->input('value');
        $kpi->employee_id = $request->input('employee');
        $kpi->save();
        return response()->json([
            'status'=>200,
            'message'=>'kpi added'
        ]);

    }
}
