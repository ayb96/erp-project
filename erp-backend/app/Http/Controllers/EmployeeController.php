<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
class EmployeeController extends Controller
{



    function getEmployee(){

        return Employee::all();
    }




    function createEmployee(Request $req){
        //from postman or frontend
        $req->validate([
            'name' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]);
        return Employee::create($req->all());


        //or

        // return Employee::create([
        //     'name' => 'Product One',
        //     'description' => 'Product-One',
        //     'price' => '99.99'
        // ]);
    }

    function getoneEmployee($id){
        return Employee::find($id);
    }

    function updateEmployee(Request $req, $id){
        $employee = Employee::find($id);
        $employee->update($req->all());
        return  $employee;
    }
    function deleteEmployee($id)
    {
        Employee::destroy($id);

    }


    function searchEmployee($key)
    {
        return Employee::where('name','Like', "%$key%")->dump()->paginate(2);

    }


}
