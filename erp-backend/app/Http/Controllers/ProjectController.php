<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Models\Project;

class ProjectController extends Controller
{
    function searchproject($key)
    {
        return Project::where('name', 'Like', "%$key%")->paginate(4);
    }
    function getProjects(){
        return Project::paginate(4);
    }
    function addProject(Request $req){
        $fields =Validator::make($req->all(),[
            'name'=>'required|string|unique:projects',
        ]);
        if($fields->fails()){
            return response()->json([
                'status'=>422,
                'errors' => $fields->errors(),
            ]);
        }
        $project = new Project;
        $project->name= $req->input('name');
        $project->save();





        return response()->json([
            'status'=>200,
            'message'=>'Project Registered Successfully'
        ]);
    }

    function getProject(){
        return Project::all();
    }

    function deleteProject($id){
        $Project = Project::find($id);
        if($Project){
            $Project->delete();
            return response()->json([
                'status'=>200,
                'message'=>'Project deleted Successfully'
            ]);
        }else{
            return response()->json([
                'status'=>404,
                'message'=>'Project Not Found'
            ]);
        }
    }

    function editProject(Request $request,$id){
        $validator  = $request->validate([
            'name'=>'required|string',

        ]);
        $project = Project::find($id);
        if($project){
            $project->name= $request->input('name');
            $project->save();
            return response()->json([
                'status'=>200,
                'message'=>'Project edited Successfully'
            ]);
        }else{
            return response()->json([
                'status' => '404',
                'errors' => 'Project not found',
            ]);
        }
    }
}
