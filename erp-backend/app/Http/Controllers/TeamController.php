<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;




class TeamController extends Controller
{
    function addTeam(Request $req){
        $fields =Validator::make($req->all(),[
            'name'=>'required|string|unique:teams',
        ]);
        if($fields->fails()){
            return response()->json([
                'status'=>422,
                'errors' => $fields->errors(),
            ]);
        }
        $team = new Team;
        $team->name= $req->input('name');
        $team->save();
        return response()->json([
            'status'=>200,
            'message'=>'Team Registered Successfully'
        ]);
    }
    function getTeams(){
        return Team::paginate(4);
    }
    function getTeam(){
        return Team::all();
    }

    function getProjectTeam(){
        return Team::with('projects')->get();
    }

    function deleteTeam($id){
        $team = Team::find($id);
        if($team){
            $team->delete();
            return response()->json([
                'status'=>200,
                'message'=>'Team deleted Successfully'
            ]);
        }else{
            return response()->json([
                'status'=>500,
                'message'=>'Team Not Found'
            ]);
        }
    }
    function editTeam(Request $request,$id){
        $validator  = $request->validate([
            'name'=>'required|string',

        ]);
//        if($validator->fails()){
//            return response()->json([
//                'status' => '422',
//                'errors' => $validator->messages(),
//
//            ]);
//        }
        $team = Team::find($id);
        if($team){
            $team->update($request->all());
//            $team->name= $request->input('name');
            $team->save();
            return response()->json([
                'status'=>200,
                'message'=>'Team edited Successfully'
            ]);
        }else{
            return response()->json([
                'status' => '404',
                'errors' => 'Team not found',
            ]);
        }
    }





}
