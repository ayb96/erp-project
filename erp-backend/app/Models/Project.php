<?php

namespace App\Models;

use App\Http\Controllers\EmployeeeController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    Protected $table = "projects";
    protected $fillable = [
        'name',
    ];

    public function employees()
    {
        return $this->belongsToMany(Employeee::class, 'employee_project', 'project_id', 'employeee_id');
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class, 'project_team', 'project_id', 'team_id');
    }


    public $timestamps=false;
}
