<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;
    Protected $table = "teams";
    protected $fillable = [
        'name',
    ];
    public $timestamps=false;
    public function employees()
    {
        return $this->hasMany(Employeee::class, 'team_id', 'id');
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_team', 'team_id', 'project_id');
    }
}
