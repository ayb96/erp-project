<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    Protected $table = "posts";
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
