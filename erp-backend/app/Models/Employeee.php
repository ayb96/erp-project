<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employeee extends Model
{
    use HasFactory;

    Protected $table = "employees";
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone_number',
        'file_path',
        'team_id',
        'project_id',
    ];
//    public function teams(){
//        return $this -> belongsTo(related:'App\Models\Team', foreignKey:'team_id', ownerKey:'id');
//    }
//    public function projects(){
//        return $this -> belongsTo(related:'App\Models\Project', foreignKey:'project_id', ownerKey:'id');
//    }
    public function teams()
    {
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }
//    public function projects()
//    {
//        return $this->belongsTo(Project::class, 'project_id', 'id');
//    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'employee_project', 'employeee_id', 'project_id');
    }

    public function kpi(){
        return $this->hasMany(Kpi::class,"employee_id","id");
    }




    public $timestamps=false;


}
