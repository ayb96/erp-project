import React, { useEffect } from "react";
import Apptest from "./Apptest";
import { Header } from "./components/Header/Header";
import { Switch, Route } from "react-router-dom";
import { AddProject } from "./components/AddProject/AddProject";
import { AddTeam } from "./components/AddTeam/AddTeam";
import { AddEmployee } from "./components/AddEmployee/AddEmployee";
import { EditEmployee } from "./components/EditEmployee/EditEmplyee";
import { EditProject } from "./components/EditProject/EditProject";
import { ProjectEmployee } from "./components/ProjectEmployee/ProjectEmployee";
import { EditTeam } from "./components/EditTeam/EditTeam";
import { EmployeeList } from "./components/EmployeeList/EmployeeList";
import { TeamEmployee } from "./components/TeamEmployee/TeamEmployee";
import { TeamList } from "./components/TeamList/TeamList";
import { ProjectList } from "./components/ProjectList/ProjectList";
import LoginPage from "./components/LoginPage/LoginPage";
import { ProtectedRoute } from "./components/ProtectedRoute/ProtectedRoute";
import { EmployeeTable } from "./components/EmployeeTable/EmployeeTable";
import { Viewemployee } from "./components/MainEmployeePage/Viewemployee";
import { AdminEmployeeTable } from "./components/DahboardEmployeeSearch/DahboardEmployeeSearch";
import { AdminRegistration } from "./components/AdminRegistration/AdminRegistration";
import { TestingPage } from "./components/TestingPage/TestingPage";
import { KpiList } from "./components/KpiList/KpiList";
import { AddKpi } from "./components/AddKpi/AddKpi";
import axios from "axios";

axios.defaults.baseURL = "http://localhost:8000/";
axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.headers.post["Accept"] = "application/json";
axios.interceptors.request.use(function (config) {
  const token = localStorage.getItem("user-info");
  console.log(token);
  config.headers.Authorization = token ? `Bearer ${token}` : ``;
  return config;
});
// axios.interceptors.response.use(response => {
//   return response;
// }, error => {
//  if (error.response.status === 401) {
//   alert('erro 401')
//  }
//  return error;
// });

axios.defaults.withCredentials = true;
const App = () => {
  return (
    <div>
      <Switch>
        <Route path="/" exact>
          <Header />
          <Viewemployee />
        </Route>
        <Route path="/test" exact>
          <Header />
          <TestingPage />
        </Route>
        <Route path="/searchemployee" exact>
          <Header />
          <EmployeeTable />
        </Route>
        <Route path="/login" exact>
          <Header />
          {/* <ProtectedRoute Cmp={LoginPage} /> */}
          <LoginPage />
        </Route>
        <Route path="/addadmin" exact>
          <Header />
          <AdminRegistration />
        </Route>
        <Route path="/addproject" exact>
          <Header />
          <ProtectedRoute Cmp={AddProject} />
        </Route>

        <Route path="/addteam" exact>
          <Header />
          <ProtectedRoute Cmp={AddTeam} />
        </Route>
        <Route path="/addemployee" exact>
          <Header />
          <ProtectedRoute Cmp={AddEmployee} />
        </Route>
        <Route path="/editemployee/:id" exact>
          <Header />
          <ProtectedRoute Cmp={EditEmployee} />
        </Route>
        <Route path="/editproject/:id" exact>
          <Header />
          <ProtectedRoute Cmp={EditProject} />
        </Route>
        <Route path="/editteam/:id" exact>
          <Header />
          <ProtectedRoute Cmp={EditTeam} />
        </Route>
        <Route path="/employeelist" exact>
          <Header />
          <ProtectedRoute Cmp={AdminEmployeeTable} />
        </Route>
        <Route path="/teamlist" exact>
          <Header />
          {/* <TeamList /> */}
          <ProtectedRoute Cmp={TeamList} />
        </Route>
        <Route path="/projectlist" exact>
          <Header />
          {/* <ProjectList /> */}
          <ProtectedRoute Cmp={ProjectList} />
        </Route>
        <Route path="/teamemployee/:id" exact>
          <Header />
          <TeamEmployee />
        </Route>
        <Route path="/projectemployee/:id" exact>
          <Header />
          <ProjectEmployee />
        </Route>
        <Route path="/addkpi" exact>
          <Header />
          <ProtectedRoute Cmp={AddKpi} />
        </Route>
        <Route path="/kpilist" exact>
          <Header />
          <ProtectedRoute Cmp={KpiList} />
        </Route>
      </Switch>

      <Apptest />
    </div>
  );
};
export default App;
