import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { AmazonContextProvider } from "./Context/AmazonContext";
import { BrowserRouter as Router } from "react-router-dom";

ReactDOM.render(
  <AmazonContextProvider>
    <React.StrictMode>
      <Router>
        <App />
      </Router>
    </React.StrictMode>
  </AmazonContextProvider>,
  document.getElementById("root")
);
