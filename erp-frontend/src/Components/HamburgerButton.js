import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useAmazonContext } from "../Context/AmazonContext";
function HamburgerButton(props) {
  const { setAuthButton, authButton } = useAmazonContext();
  const history = useHistory();
  // useEffect(() => {

  // }, [])

  const renderUpdateForm = () => {
    return (
      <div style={{ fontSize: "45px", position:'relative',bottom:'14px' }} onClick={props.click}>
        &#9776;
      </div>
    );
  };

  const renderCourse = () => {
    return (
      <div>
        <button
          className="searchyespleasebro headerbuttonz"
          onClick={() => history.push("/")}
        >
          Home
        </button>

        <button
          className="searchyespleasebro headerbuttonzz"
          onClick={() => history.push("/searchemployee")}
        >
          Search
        </button>
      </div>
    );
  };

  let AuthButtonz = "";
  if (authButton) {
    AuthButtonz = renderUpdateForm();
  } else if(!authButton && !localStorage.getItem("user-info")){
    AuthButtonz = renderCourse();
  }

  return <div className="hamburger-btn">{AuthButtonz}</div>;
}

export default HamburgerButton;
