import React, { useEffect, useState } from "react";
import SideNavRow from "./SideNavRow";
import DropDown from "./DropDowns/DropDown";
import { useAmazonContext } from "../Context/AmazonContext";
import { useHistory } from "react-router-dom";

function SideNavContent(props) {
  let history = useHistory();
  const [dropDownEntries, setDropDownEntries] = useState(null);

   const { setFixme } = useAmazonContext();

  const Testingz = [
    {
      entries: [
        {
          title: "Employee",
          entries: [
            {
              title: "Manage Employee",
              entries: [
                {
                  id: 1,
                  title: "Employee List",
                },
                {
                  id: 2,
                  title: "Add Employee",
                },
                
              ],
            },
          ],
        },
        {
          title: "Team",
          entries: [
            {
              title: "Manage Teams",
              entries: [
                {
                  id: 4,
                  title: "Team List",
                },
                {
                  id: 5,
                  title: "Add Team",
                },
                
              ],
            },
          ],
        },
        {
          title: "Project",
          entries: [
            {
              title: "Manage Projects",
              entries: [
                {
                  id: 7,
                  title: "Project List",
                },
                {
                  id: 8,
                  title: "Add Project",
                },
                
              ],
            },
          ],
        },
      ],
      _id: "60ab69ee0a5705404d68d9fc",
      title: "Digital Content And Devices",
      type: {
        rows: true,
      },
    },
    {
      entries: [
        {
          title: "Kpi",
          entries: [
            {
              title: "Manage Kpi",
              entries: [
                {
                  id: 1,
                  title: "Kpi List",
                },
                {
                  id: 2,
                  title: "Add Kpi"
                },
                
              ],
            },
          ],
        },
        
        
      ],
      _id: "60ab69ee0a5705404d68d9fc",
      title: "Kpi's Control",
      type: {
        rows: true,
      },
    },
    
    {
      entries: [
        {
          title: "Add Admin",
          
        },
      ],
      _id: "60a3b6a570a5705404d68d9ff",
      title: "Admins",
      type: {
        rows: false,
      },
    },
    {
      entries: [
        {
          title: "Your Account",
        },
        {
          title: "Currency Settings",
        },
        {
          title: "Customer Service",
        },
      ],
      _id: "60ab6a570a5705404d68d9ff",
      title: "Help & Settings",
      type: {
        rows: false,
      },
    },
  ];

  const findsome = (val) => {
    switch (val) {
      case "Add Admin":
        history.push("/addadmin");
        break;
    }
  };

  return (
    <div
      className="sideNavContainer"
      style={
        props.state === "exiting"
          ? { animation: "moveMainContainer .3s forwards" }
          : props.state === "entering"
          ? { animation: "moveMainContainer .3s reverse backwards" }
          : null
      }
    >
      {Testingz &&
        Testingz.map((entry, index) => {
          return (
            <>
              <div className="sidenavContentHeader">{entry.title}</div>
              {entry.entries.map((subEntry, index) => {
                // if (subEntry.type) {
                //   !dropDownEntries && setDropDownEntries(index);
                //   return <DropDown entries={entry.entries.slice(index + 1)} />;
                // if (dropDownEntries && index < dropDownEntries) {
                return !entry.type.rows ? (
                  <a href="#">
                    <div
                      className="sidenavContent"
                      onClick={() => findsome(subEntry.title)}
                    >
                      {subEntry.title}
                    </div>
                  </a>
                ) : (
                  <SideNavRow
                    text={subEntry.title}
                    entries={subEntry.entries}
                  />
                );
              })}
              {index !== Testingz.length  && <hr />}
            </>
          );
        })}
      <div style={{ minHeight: "60px" }}></div>
    </div>
  );
}

export default SideNavContent;
