import React, { useState } from "react";
import { useAmazonContext } from "../Context/AmazonContext";
import { useHistory } from "react-router-dom";
function SideNavRow(props) {
  let history = useHistory();
  const { setSubContainer, setSubContainerEntries, setNavOpen } =
    useAmazonContext();
  const [sata, setSata] = useState([props.entries]);
  const openRow = () => {
    setSubContainer(true);
    setSubContainerEntries(props.entries);
  };

  const closeNav = () => {
    setNavOpen(false);
  };
  const goto = () => {
    switch (props.text) {
      case "Add Employee":
        history.push("/addemployee");
        break;
      case "Add Team":
        history.push("/addteam");
        break;
      case "Add Project":
        history.push("/addproject");
        break;

      case "Employee List":
        history.push("/employeelist");
        break;
      case "Team List":
        history.push("/teamlist");
        break;
      case "Project List":
        history.push("/projectlist");
        break;
      case "Add Admin":
        closeNav();
        history.push("/addadmin");
        break;
      case "Add Kpi":
        closeNav();
        history.push("/addkpi");
        break;
      case "Kpi List":
        closeNav();
        history.push("/kpilist");
        break;
    }
  };

  return (
    <div className="sidenavRow" onClick={() => props.entries && openRow()}>
      <div>
        {props.text}
        <button onClick={() => goto()} className="sidebar_wrap"></button>
      </div>
      <i class="fas fa-chevron-right"></i>
    </div>
  );
}

export default SideNavRow;
