import React, { useContext, useState, useEffect } from "react";

const AmazonContext = React.createContext();

export function useAmazonContext() {
  return useContext(AmazonContext);
}

export function AmazonContextProvider(props) {
  //Responsible for opening and closing the sub container
  const [subContainer, setSubContainer] = useState(false);
  //Responsible for storing the subContainers entries
  const [subContainerEntries, setSubContainerEntries] = useState(null);
  //Responsible for holding all of the data that goes into the navbar
  const [entryStore, setEntryStore] = useState(null);
  const [testz, setTestz] = useState(null);
  const [fixme, setFixme] = useState(null);
  const [navOpen, setNavOpen] = useState(false);
  // useEffect(() => {
  //   fetch("https://amazon-navbar.herokuapp.com/fetch")
  //     .then((data) => data.json())
  //     .then((response) => {
  //       setEntryStore(response);
  //     });
  // }, []);

  const [authButton, setAuthButton] = useState(false);

  const value = {
    subContainer,
    setSubContainer,
    subContainerEntries,
    setSubContainerEntries,
    entryStore,
    setEntryStore,
    fixme,
    setFixme,
    testz,
    setTestz,
    navOpen,
    setNavOpen,
    authButton,
    setAuthButton,
  };

  return (
    <AmazonContext.Provider value={value}>
      {props.children}
    </AmazonContext.Provider>
  );
}
