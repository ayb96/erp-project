import React, { useState, useEffect } from "react";
import swal from "sweetalert";
import axios from "axios";
export const AddKpi = () => {
  const [name, setName] = useState("");
  const [value, setValue] = useState("");
  const [employee, setEmployee] = useState("");
  const [allemployee, setAllemployee] = useState([]);
  const [error, setError] = useState("");
  useEffect(() => {
    async function getemployee() {
      const req = await axios.get("api/employeez");
      setAllemployee(req.data);
    }
    getemployee();
  }, []);

  const addkpi = () => {
    const item = { name, value, employee };
    axios
      .post("/api/addkpi", item)

      .then((res) => {
        if (res.data.status === 200) {
          swal({
            title: "Good job!",
            text: "Kpi Created",
            icon: "success",
            button: "Ayoub the best",
          });
          setError('')
        } else if (res.data.status === 422) {
          setError(res.data.errors);
        }
      });
  };
  return (
    <div>
      <div className="addemplyeecontainer">
        <div className="registrationcontainer">
          <div className="registrationbox regtitle">Add Kpi</div>
          <div className="registrationbox">
            <label for="projectname">Kpi Name</label>
            <br />
            <input
              type="text"
              placeholder="Enter your Kpi name"
              name="projectname"
              id="projectname"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <span>{error.name}</span>
          </div>
          <div className="registrationbox">
            <label for="projectname">Value</label>
            <br />
            <input
              type="text"
              placeholder="Enter Kpi value"
              name="projectname"
              id="projectname"
              value={value}
              onChange={(e) => setValue(e.target.value)}
            />
            <span>{error.value}</span>
          </div>
          <div>
            <select
              style={{ width: "150px" }}
              className="registrationselect"
              value={employee}
              onChange={(e) => setEmployee(e.target.value)}
              name="team"
            >
              <option value="A">Select Employee</option>
              {allemployee.map((vall) => (
                <option value={vall.id}>{vall.first_name}</option>
              ))}
            </select>
            <span
              className="fixselect"
              style={{
                backgroundColor: "rgb(236, 89, 89)",
                marginLeft: "3px",
                borderRadius: "5px",
                color: "white",
              }}
            >
              {error.employee}
            </span>
          </div>

          <div className="registrationbox">
            <button type="submit" className="regsub" onClick={addkpi}>
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
