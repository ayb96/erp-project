import React, { useState, useEffect } from "react";

import SearchIcon from "@material-ui/icons/Search";
import swal from "sweetalert";
import { useHistory, Link } from "react-router-dom";
import { useAmazonContext } from "../../Context/AmazonContext";
import "bootstrap/dist/css/bootstrap.min.css";
import Pagination from "react-js-pagination";
import axios from "axios";
export const AdminEmployeeTable = () => {
  const { testz, setTestz } = useAmazonContext();
  const [filter, setFilter] = useState("");
  const [team, setTeam] = useState("");
  const [project, setProject] = useState("");
  const [data, setData] = useState([]);
  const [dataz, setDataz] = useState([]);
  const [current_page, setCurrent_page] = useState("");
  const [total, setTotal] = useState("");
  const [teamdata, setTeamdata] = useState([]);
  const [projectdata, setProjectdata] = useState([]);
  const [per_page, setPer_page] = useState("");
  const history = useHistory();
  useEffect(() => {
    fetchData();
  }, []);
  useEffect(() => {
    async function fetchDataT() {
      const req = await axios.get("api/team");
      setTeamdata(req.data);
    }
    async function fetchDataP() {
      const req = await axios.get("api/project");
      setProjectdata(req.data);
      console.log(projectdata);
    }
    fetchDataP();
    fetchDataT();
  }, []);

  async function fetchData(pageNumber = 1) {
    const req = await axios.get(`api/allemployee?page=${pageNumber}`);
    setDataz(req.data.data);
    setCurrent_page(req.data.current_page);
    setTotal(req.data.total);
    setPer_page(req.data.per_page);
  }

  async function searching(key, pageNumber = 1) {
    let result = await fetch(
      "http://localhost:8000/api/search/" + key + `?page=${pageNumber}`
    );
    result = await result.json();
    setData(result.data);
    setCurrent_page(result.current_page);
    setTotal(result.total);
    setPer_page(result.per_page);
  }

  const deleteemployee = (e, id) => {
    e.preventDefault();
    const thisClicked = e.currentTarget;
    thisClicked.innerText = "Deleting";
    axios
      .delete(`/api/delete-employee/${id}`)

      .then((res) => {
        if (res.data.status === 200) {
          swal({
            title: "Good job!",
            text: res.data.message,
            icon: "success",
            button: "Ayoub the best",
          });
          thisClicked.closest("tr").remove();
        } else if (res.data.status === 404) {
          swal({
            title: "Try again!",
            text: res.data.message,
            icon: "error",
          });
          thisClicked.innerText = "delete";
        }
      });
  };
  return (
    <div>
      <div className="flexmeplz">
        <div className="employeelist-charts">
          <input
            type="email"
            placeholder="Enter your email"
            className="loginemail "
            name="adminemail"
            id="adminemail"
            onChange={(e) => {
              searching(e.target.value);
              setFilter(e.target.value);
            }}
            required
          />
          <SearchIcon className="loginiconzemloyeelist" />
        </div>
        <select
          className="registrationselectproject"
          value={team}
          onChange={(e) => setTeam(e.target.value)}
          name="team"
        >
          <option value="A">Select Team</option>
          {teamdata.map((vall) => (
            <option value={vall.id}>{vall.name}</option>
          ))}
        </select>
        <select
          className="registrationselectproject"
          value={project}
          onChange={(e) => setProject(e.target.value)}
          name="project"
        >
          <option value="A">Select Project</option>
          {projectdata.map((vall) => (
            <option value={vall.id}>{vall.name}</option>
          ))}
        </select>
      </div>
      <table>
        <thead>
          <tr>
            <th>Profile Picture</th>
            <th>First Name</th>
            <th>Last Name </th>
            <th>Email</th>
            <th>Phone Number</th>
            <th>Team id</th>
            <th>Project id</th>

            <th>Operations</th>
          </tr>
        </thead>
        <tbody>
          {filter == ""
            ? dataz.map((item, index) => (
                <tr key={index}>
                  <td>
                    <img
                      style={{ width: 100, height: 100 , borderRadius:'50%', borderColor:'#1dc4e7'}}
                      src={"http://localhost:8000/" + item.file_path}
                      alt=""
                    />
                  </td>
                  <td>{item.first_name}</td>
                  <td>{item.last_name}</td>
                  <td>{item.email}</td>
                  <td>{item.phone_number}</td>
                  <td>{item.team_id}</td>
                  <td>{item.project_id}</td>

                  <td>
                    <span
                      id="deleteproduct"
                      onClick={(e) => deleteemployee(e, item.id)}
                    >
                      Delete
                    </span>
                    <Link to={"editemployee/" + item.id}>
                      <span id="editproduct">Edit</span>
                    </Link>
                  </td>
                </tr>
              ))
            : data.map((item, index) => (
                <tr key={index}>
                  <td>
                    <img
                      style={{ width: 100, height: 100 , borderRadius:'50%', borderColor:'#1dc4e7'}}
                      src={"http://localhost:8000/" + item.file_path}
                      alt=""
                    />
                  </td>
                  <td>{item.first_name}</td>
                  <td>{item.last_name}</td>
                  <td>{item.email}</td>
                  <td>{item.phone_number}</td>
                  <td>{item.team_id}</td>
                  <td>{item.project_id}</td>
                  
                  <td>
                    <span
                      id="deleteproduct"
                      onClick={(e) => deleteemployee(e, item.id)}
                    >
                      Delete
                    </span>
                    <Link to={"editemployee/" + item.id}>
                      <span id="editproduct">Edit</span>
                    </Link>
                  </td>
                </tr>
              ))}
        </tbody>
      </table>
      {filter == "" ? (
        <div className="mt-3 pagina">
          <Pagination
            activePage={current_page}
            totalItemsCount={total}
            itemsCountPerPage={per_page}
            onChange={(pageNumber) => fetchData(pageNumber)}
            itemClass="page-item"
            linkClass="page-link"
            firstPageText="First"
            lastPageText="Last"
          />
        </div>
      ) : (
        <div className="mt-3 pagina">
          <Pagination
            activePage={current_page}
            totalItemsCount={total}
            itemsCountPerPage={per_page}
            onChange={(pageNumber) => searching(filter, pageNumber)}
            itemClass="page-item"
            linkClass="page-link"
            firstPageText="First"
            lastPageText="Last"
          />
        </div>
      )}
    </div>
  );
};
