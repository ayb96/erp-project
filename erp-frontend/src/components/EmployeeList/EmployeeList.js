import React, { useEffect } from "react";
import "./EmployeeList.css";
import { BarChart } from "../Charts/BarChart/BarChart";
import { PieChart } from "../Charts/PieChart/PieChart";
import { LineChart } from "../Charts/LineChart/LineChart";
import { RadarChart } from "../Charts/RadarChart/RadarChart";
import SearchIcon from "@material-ui/icons/Search";
import {Helmet} from "react-helmet";
export const EmployeeList = () => {

  return (
    <div>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Search panel</title>
        <link rel="canonical" href="http://mysite.com/example" />
        <meta name="description" content="Erp application" />
      </Helmet>
      <input type="checkbox" id="check" />
      <h1>Employee List</h1>
      <label for="check">
        <div className="employeelist-charts">
          <input
            type="email"
            placeholder="Enter your email"
            className="loginemail"
            name="adminemail"
            id="adminemail"
            required
          />
          <SearchIcon className="loginiconzemloyeelist" />
        </div>
      </label>
      <div className="chartContainer">
        <BarChart />
        <LineChart />
        <PieChart />
        <RadarChart />
      </div>
    </div>
  );
};
