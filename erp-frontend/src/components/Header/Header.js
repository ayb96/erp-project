import React from "react";
import "./Header.css";
import Skeleton from "react-loading-skeleton";
import { useHistory } from "react-router-dom";
import axios from "axios";
import swal from "sweetalert";
import { useAmazonContext } from "../../Context/AmazonContext";
export const Header = () => {
  const { setAuthButton, authButton } = useAmazonContext();
  const history = useHistory();

  // const logouttologin = () => {
  //   localStorage.clear();
  //   history.push("/login");
  // };

  async function logouttologin(e) {
    e.preventDefault();

    // axios.get("/sanctum/csrf-cookie").then((response) => {
    //   axios
    //     .post("/api/logout")

    //     .then((res) => {
    //       if (res.data.status === 200) {
    localStorage.removeItem("user-info");
    localStorage.removeItem("auth_username");

    history.push("/");
    setAuthButton(false);
    //   swal({
    //     title: "Good job!",
    //     text: "You are logged Out",
    //     icon: "success",
    //     button: "Ayoub the best",
    //   });
    // } else if (res.data.status === 401) {
    //   swal({
    //     title: "Try again!",
    //     text: "Error",
    //     icon: "error",
    //   });
    //         }
    //       });
    //   });
  }

  const renderUpdateForm = () => {
    return (
      <button className="logout-bnt" onClick={logouttologin}>
        logout
      </button>
    );
  };

  const renderCourse = () => {
    return (
      <div>
        {/* <button className="eeeeeee">Home</button> */}
        <button className="logout-bnt" onClick={() => history.push("/login")}>
          Sign In
        </button>
      </div>
    );
  };
  let AuthButtonz = "";
  if (authButton) {
    AuthButtonz = renderUpdateForm();
  } else {
    AuthButtonz = renderCourse();
  }
  return (
    <div className="header-container">
      <h1 className="header-title">
        <p style={{ cursor: "pointer" }} onClick={() => history.push("/")}>
          Erp{" "}
          {AuthButtonz && !localStorage.getItem("user-info") ? (
            <span>Website</span>
          ) : (
            <span>Dashboard</span>
          )}
        </p>
      </h1>
      {AuthButtonz}
    </div>
  );
};
