import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import swal from "sweetalert";
import Pagination from "react-js-pagination";
import SearchIcon from "@material-ui/icons/Search";
import "./ProjectList.css";
export const ProjectList = () => {
  const [data, setData] = useState(null);
  const [current_page, setCurrent_page] = useState("");
  const [total, setTotal] = useState("");
  const [per_page, setPer_page] = useState("");
  const [filter, setFilter] = useState("");
  const [searchdata, setSearchdata] = useState([]);
  const [normaldata, setNormaldata] = useState([]);
  const [loading, setLoading] = useState(true);
  const [teamdata, setTeamdata] = useState([]);
  const [projectdata, setProjectdata] = useState([]);
  const [team, setTeam] = useState("");
  const [project, setProject] = useState("");
  useEffect(() => {
    async function fetchDataT() {
      const req = await axios.get("api/team");
      setTeamdata(req.data);
    }
    async function fetchDataP() {
      const req = await axios.get("api/project");
      setProjectdata(req.data);
      console.log(projectdata);
    }
    fetchDataP();
    fetchDataT();
  }, []);
  useEffect(() => {
    fetchData();
  }, []);
  // useEffect(() => {

  //   if (filter == "") {
  //     setData(normaldata);
  //     console.log(data);
  //   } else if (filter != "") {
  //     setData(searchdata);
  //     console.log(data);
  //   }
  // });
  async function searchingproject(key, pageNumber = 1) {
    let result = await fetch(
      "http://localhost:8000/api/searchproduct/" + key + `?page=${pageNumber}`
    );
    result = await result.json();
    
    setLoading(false);
    setSearchdata(result.data);
    setCurrent_page(result.current_page);
    setTotal(result.total);
    setPer_page(result.per_page);
  }
  
  async function fetchData(pageNumber = 1) {
    const req = await axios.get(`api/projects?page=${pageNumber}`);
    setNormaldata(req.data.data);
    console.log(req.data);
    setCurrent_page(req.data.current_page);
    setTotal(req.data.total);
    setPer_page(req.data.per_page);
    setLoading(false);
  }
  const deleteproject = (e, id) => {
    e.preventDefault();
    const thisClicked = e.currentTarget;
    thisClicked.innerText = "Deleting";
    axios
      .delete(`/api/delete-project/${id}`)

      .then((res) => {
        if (res.data.status === 200) {
          swal({
            title: "Good job!",
            text: res.data.message,
            icon: "success",
            button: "Ayoub the best",
          });
          thisClicked.closest("tr").remove();
        }
        // else if (res.data.status === 404) {
        //   swal({
        //     title: "Try again!",
        //     text: res.data.message,
        //     icon: "error",
        //   });
        //   thisClicked.innerText = "delete";
        // }
      })
      .catch((error) => {
        swal({
          title: "Try again!",
          text: "Cannot delete Project",
          icon: "error",
        });
        thisClicked.innerText = "delete";
      });
  };
  if (loading) {
    return <h1 style={{ marginTop: "200px" }}>loading...</h1>;
  }

  return (
    <div>
      <div className="flexmeplz">
        {/* <input
          name="searching"
          className="searchproject"
          type="text"
          onChange={(e) => {
            searchingproject(e.target.value);
            setFilter(e.target.value);
          }}
        /> */}

        <div className="employeelist-charts ">
          <input
            type="email"
            placeholder="Enter your email"
            className="loginemail "
            name="adminemail"
            id="adminemail"
            onChange={(e) => {
              searchingproject(e.target.value);
              setFilter(e.target.value);
            }}
          />
          <SearchIcon className="loginiconzemloyeelist" />
        </div>
        

        <select
          className="registrationselectproject"
          value={team}
          onChange={(e) => setTeam(e.target.value)}
          name="team"
        >
          <option value="A">Select Team</option>
          {teamdata.map((vall) => (
            <option value={vall.id}>{vall.name}</option>
          ))}
        </select>
        <select
          className="registrationselectproject"
          value={project}
          onChange={(e) => setProject(e.target.value)}
          name="project"
        >
          <option value="A" defaultChecked>Select Employee</option>
          {projectdata.map((val) => (
            <option value={val.id}>{val.name}</option>
          ))}
        </select>
      </div>
      <table>
        <thead>
          <tr>
            <th className="edittabletitle">Project Name</th>

            <th className="edittabletitle">Operations</th>
          </tr>
        </thead>
        <tbody>
          {filter == ""
            ? normaldata
            
            .map((item, index) => (
                <tr key={index}>
                  <td>{item.name}</td>
                  <td className="opedit">
                    <span
                      id="deleteproduct"
                      onClick={(e) => deleteproject(e, item.id)}
                    >
                      Delete
                    </span>
                    <Link to={"editproject/" + item.id}>
                      <span id="editproduct">Edit</span>
                    </Link>
                    <Link to={"projectemployee/" + item.id}>
                      <span id="viewproduct">View</span>
                    </Link>
                  </td>
                </tr>
              ))
            : searchdata.map((item, index) => (
                <tr key={index}>
                  <td>{item.name}</td>
                  <td className="opedit">
                    <span
                      id="deleteproduct"
                      onClick={(e) => deleteproject(e, item.id)}
                    >
                      Delete
                    </span>
                    <Link to={"editproject/" + item.id}>
                      <span id="editproduct">Edit</span>
                    </Link>
                    <Link to={"projectemployee/" + item.id}>
                      <span id="viewproduct">View</span>
                    </Link>
                  </td>
                </tr>
              ))}
        </tbody>
      </table>
      {filter == "" ? (
        <div className="mt-3 pagina">
          <Pagination
            activePage={current_page}
            totalItemsCount={total}
            itemsCountPerPage={per_page}
            onChange={(pageNumber) => fetchData(pageNumber)}
            itemClass="page-item"
            linkClass="page-link"
            firstPageText="First"
            lastPageText="Last"
          />
        </div>
      ) : (
        <div className="mt-3 pagina">
          <Pagination
            activePage={current_page}
            totalItemsCount={total}
            itemsCountPerPage={per_page}
            onChange={(pageNumber) => searchingproject(filter, pageNumber)}
            itemClass="page-item"
            linkClass="page-link"
            firstPageText="First"
            lastPageText="Last"
          />
        </div>
      )}
    </div>
  );
};
