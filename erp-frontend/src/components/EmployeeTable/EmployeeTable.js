import React, { useState, useEffect } from "react";
import "./EmployeeTable.scss";
import SearchIcon from "@material-ui/icons/Search";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { useHistory, Link } from "react-router-dom";

export const EmployeeTable = () => {
  const [data, setData] = useState([]);
  const history = useHistory();

  async function searching(key) {
    // let result = await fetch("http://localhost:8000/api/search/"+key)
    // result = await result.json()
    // setData(result)
    let result = await fetch("http://localhost:8000/api/list");
    result = await result.json();
    setData(result);
  }
  return (
    <div>
      <button className="backbutton" onClick={() => history.push("/")}>
        <ArrowBackIosIcon />
        <span>Back</span>
      </button>
      <div >
        <div className="employeelist-charts ">
          <input
            type="email"
            placeholder="Enter your email"
            className="loginemail "
            name="adminemail"
            id="adminemail"
            onChange={(e) => searching(e.target.value)}
            required
          />
          <SearchIcon className="loginiconzemloyeelist" />
        </div>
        <table>
          <thead>
            <tr>
              <th>First Name</th>
              <th>Last Name </th>
              <th>Email</th>
              <th>Phone Number</th>
              <th>File_Path</th>
            </tr>
          </thead>
          <tbody>
            {data.map((item, index) => (
              <tr key={index}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.price}</td>
                <td>{item.discription}</td>
                <td>
                  <img
                    style={{ width: 100, height: 100 }}
                    src={"http://localhost:8000/" + item.file_path}
                    alt=""
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};
