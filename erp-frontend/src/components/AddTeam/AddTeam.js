import React, { useState } from "react";
import swal from "sweetalert";
import axios from "axios";
export const AddTeam = () => {
  const [name, setName] = useState("");
  const [error, setError] = useState('')
  const addteam = () => {
    const item = { name };
    axios
    .post("/api/addteam", item)

    .then((res) => {
      if (res.data.status === 200) {
        swal({
          title: "Good job!",
          text: "Team Created",
          icon: "success",
          button: "Ayoub the best",
        });
        setError('')
      } else if (res.data.status === 422){
        console.log(res)
        setError(res.data.errors)
      }
    });
  };
  return (
    <div>
      <div className="registrationcontainer">
        <div className="registrationbox regtitle">Add Team</div>
        <div className="registrationbox">
          <label for="teamname">Team Name</label>
          <br />
          <input
            type="text"
            placeholder="Enter team name"
            name="teamname"
            id="teamname"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <span>{error.name}</span>
        </div>

        <div className="registrationbox">
          <button type="submit" className="regsub" onClick={addteam}>
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};
