import React, { useState } from "react";
import swal from "sweetalert";
import axios from "axios";
export const AddProject = () => {
  const [name, setName] = useState("");
  const [error, setError] = useState('')
  const addproject = () => {
   
    const item = { name };
    axios
      .post("/api/addproject", item)

      .then((res) => {
        if (res.data.status === 200) {
          swal({
            title: "Good job!",
            text: "Project Created",
            icon: "success",
            button: "Ayoub the best",
          });
          setError('')
        } else  if (res.data.status === 422){
          setError(res.data.errors)
          
        }
      });
  };
  return (
    <div>
      <h1>Add Project</h1>
      <div className="addemplyeecontainer">
        <div className="registrationcontainer">
          <div className="registrationbox regtitle">Add Project</div>
          <div className="registrationbox">
            <label for="projectname">Project Name</label>
            <br />
            <input
              type="text"
              placeholder="Enter your Project name"
              name="projectname"
              id="projectname"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
            <span>{error.name}</span>
          </div>

          <div className="registrationbox">
            <button type="submit" className="regsub" onClick={addproject}>
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
