import React, { useState } from "react";
import swal from "sweetalert";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
export const EditTeam = () => {
  const [name, setName] = useState("");
  const { id } = useParams();
  const editteam = () => {
    // const formdata = new FormData();

    // formdata.append("name", name);
    const item = {
      name,
    };
    axios
      .put(`/api/edit-team/${id}`, item)

      .then((res) => {
        if (res.data.status === 200) {
          // setError([]);

          swal({
            title: "Good job!",
            text: res.data.message,
            icon: "success",
            button: "Ayoub the best",
          });
        } else if (res.data.status === 422) {
          // setError(res.data.error);
          swal({
            title: "Try again!",
            text: "All field are mandetory",
            icon: "error",
          });
        } else if (res.data.status === 404) {
          // setError(res.data.error);
          swal({
            title: "Try again!",
            text: res.data.message,
            icon: "error",
          });
        }
      });
  };

  return (
    <div>
      <div className="registrationcontainer">
        <div className="registrationbox regtitle">Edit Team</div>
        <div className="registrationbox">
          <label for="editteam">Team Name</label>
          <br />
          <input
            type="text"
            placeholder="Enter your team name"
            name="name"
            id="editteam"
            onChange={(e) => setName(e.target.value)}
          />
        </div>

        <div className="registrationbox">
          <button type="submit" className="regsub" onClick={editteam}>
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};
