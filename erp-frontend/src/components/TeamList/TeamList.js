import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./TeamList.css";
import swal from "sweetalert";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import Pagination from "react-js-pagination";
export const TeamList = () => {
  const [data, setData] = useState([]);
  const [current_page, setCurrent_page] = useState("");
  const [total, setTotal] = useState("");
  const [per_page, setPer_page] = useState("");
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    fetchData();
  }, []);

  async function fetchData(pageNumber = 1) {
    const req = await axios.get(`api/teams?page=${pageNumber}`);
    setData(req.data.data);
    console.log(req.data);
    setCurrent_page(req.data.current_page);
    setTotal(req.data.total);
    setPer_page(req.data.per_page);
    setLoading(false)
  }

  const deleteteam = (e, id) => {
    e.preventDefault();
    const thisClicked = e.currentTarget;
    thisClicked.innerText = "Deleting";
    axios
      .delete(`/api/delete-team/${id}`)

      .then((res) => {
        if (res.data.status === 200) {
          swal({
            title: "Good job!",
            text: res.data.message,
            icon: "success",
            button: "Ayoub the best",
          });
          thisClicked.closest("tr").remove();
        }
        // else if (res.data.status === 500) {
        //   swal({
        //     title: "Try again!",
        //     text: res.data.message,
        //     icon: "error",
        //   });
        //   thisClicked.innerText = "delete";
        // }
      })
      .catch((error) => {
        swal({
          title: "Try again!",
          text: "Cannot delete Team",
          icon: "error",
        });
        thisClicked.innerText = "delete";
      });
  };
  if(loading){
    <h1>loading...</h1>
  }
  return (
    <div>
      
      <table>
        <thead>
          <tr>
            <th className="edittabletitle">Team Name</th>

            <th className="edittabletitle">Operations</th>
          </tr>
        </thead>
        <tbody>
          {data.map((item, index) => (
            <tr key={index}>
              <td>{item.name}</td>
              <td className="opedit">
                <span
                  id="deleteproduct"
                  onClick={(e) => deleteteam(e, item.id)}
                >
                  Delete
                </span>
                <Link to={"editteam/" + item.id}>
                  <span id="editproduct">Edit</span>
                </Link>
                <Link to={"teamemployee/" + item.id}>
                  <span id="viewproduct">View</span>
                </Link>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="mt-3 pagina">
        <Pagination
          activePage={current_page}
          totalItemsCount={total}
          itemsCountPerPage={per_page}
          onChange={(pageNumber) => fetchData(pageNumber)}
          itemClass="page-item"
          linkClass="page-link"
          firstPageText="First"
          lastPageText="Last"
        />
      </div>
    </div>
  );
};
