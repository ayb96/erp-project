import React, { useState, useEffect } from "react";
import "./LoginPage.css";
import PersonIcon from "@material-ui/icons/Person";
import LockIcon from "@material-ui/icons/Lock";
import axios from "axios";
import { useHistory } from "react-router-dom";
import swal from "sweetalert";
import { useAmazonContext } from "../../Context/AmazonContext";

import { withRouter } from "react-router-dom";

const LoginPage = () => {
  const { testz, setTestz, setAuthButton, authButton } = useAmazonContext();
  const history = useHistory();

  useEffect(() => {
    document.title = "Login Page";
    if (localStorage.getItem("user-info")) {
      history.push("/Employeelist");
    }
  }, []);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  async function logIn(e) {
    e.preventDefault();
    const item = { email, password };
    axios.get("/sanctum/csrf-cookie").then((response) => {
      axios
        .post("/api/login", item)

        .then((res) => {
          if (res.data.status === 200) {
            // setAuthButton(true);
            // console.log(testz);
            setTestz(res.data.token);
            localStorage.setItem("user-info", res.data.token);
            localStorage.setItem("auth_username", res.data.username);
            console.log(testz);
            history.push("/employeelist");
            swal({
              title: "Good job!",
              text: "You are logged In",
              icon: "success",
              button: "Ayoub the best",
            });
          } else if (res.data.status === 401) {
            swal({
              title: "Try again!",
              text: "Invalid Email or Password",
              icon: "error",
            });
          }
        });
    });
  }
  return (
    <div>
      <form onSubmit={logIn}>
        <div className="logincontainer">
          <div className="logincontainer1">
            <h2 id="h2oflogin">Login Page</h2>
            <div className="loginicon">
              <PersonIcon className="loginiconz" />
              <input
                type="email"
                placeholder="Enter your email"
                className="loginemail"
                name="adminemail"
                id="adminemail"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </div>

            <div className="loginicon">
              <LockIcon className="loginiconz" />
              <input
                type="Password"
                placeholder="Enter your password"
                className="loginpassword"
                name="adminpassword"
                id="adminpassword"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </div>
            <button type="submit">Login</button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default withRouter(LoginPage);
