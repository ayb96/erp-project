import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./AdminRegistration.css";
import swal from "sweetalert";
import axios from "axios";
export const AdminRegistration = () => {
  const history = useHistory();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState('')

  async function signUp(e) {
    e.preventDefault();
    const item = { name, email, password };
    console.log(item);
    axios.defaults.withCredentials = true;
    axios
      .get("/sanctum/csrf-cookie")
      .then((res) => {
        axios
          .post("/api/authregister", item)

          .then((res) => {
            if (res.data.status === 200) {
              swal({
                title: "Good job!",
                text: "Admin Created",
                icon: "success",
                button: "Ayoub the best",
              });
              setError('')
            } else if (res.data.status === 422){
              setError(res.data.errors)
            }
          });
      })
      .catch((err) => {
        swal({
          title: "Try again!",
          text: "You can not access",
          icon: "error",
        });
      });
  }

  return (
    <div className="addemplyeecontainer">
      <div className="registrationcontainer">
        <div className="registrationbox regtitle">Add Admin</div>
        <div className="registrationbox">
          <label for="name">Name</label>
          <br />
          <input
            type="text"
            placeholder="Enter admin name"
            name="name"
            id="name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <span>{error.name}</span>
        </div>
        <div className="registrationbox">
          <label for="email">Email</label>
          <br />
          <input
            type="email"
            placeholder="Enter admin email"
            name="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
           <span>{error.email}</span>
        </div>
        <div className="registrationbox">
          <label for="password">password</label>
          <br />
          <input
            type="password"
            placeholder="Enter password"
            name="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
           <span>{error.password}</span>
        </div>

        <div className="registrationbox">
          <button type="submit" className="regsub" onClick={signUp}>
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};
