import React, {useEffect} from "react";
import "./Viewemployee.css";
import { BarChart } from "../Charts/BarChart/BarChart";
import { PieChart } from "../Charts/PieChart/PieChart";
import { LineChart } from "../Charts/LineChart/LineChart";
import { RadarChart } from "../Charts/RadarChart/RadarChart";
import {Helmet} from "react-helmet";

export const Viewemployee = () => {

  return (
    <div>
       <Helmet>
        <meta charSet="utf-8" />
        <title>Erp Web</title>
        <link rel="canonical" href="http://mysite.com/example" />
        <meta name="description" content="Erp application" />
      </Helmet>
      <div className="chartContainer">
        <BarChart />
        <LineChart />
        <PieChart />
        <RadarChart />
      </div>
    </div>
  );
};
