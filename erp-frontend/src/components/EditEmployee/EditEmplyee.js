import React, { useState, useEffect } from "react";
import "./EditEmployee.css";
import swal from "sweetalert";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import spinz from "../../bower_components/SVG-Loaders/svg-loaders/tail-spin.svg";
export const EditEmployee = (props) => {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [phonenumber, setPhonenumber] = useState("");
  const [email, setEmail] = useState("");
  const [team, setTeam] = useState("");
  const [project, setProject] = useState("");
  const [file, setFile] = useState("");
  const [teamdata, setTeamdata] = useState([]);
  const [projectdata, setProjectdata] = useState([]);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState([]);
  const history = useHistory();
  const { id } = useParams();
  console.log(id);
  useEffect(() => {
    console.log(id);
    async function fetchData() {
      const req = await axios.get("api/team");

      setTeamdata(req.data);
    }
    async function fetchDataP() {
      const req = await axios.get("api/project");
      setProjectdata(req.data);
    }
    fetchDataP();
    fetchData();

    axios.get(`/api/edit-employee/${id}`).then((res) => {
      if (res.data.status === 200) {
        console.log(res.data.employee);
        setData(res.data.employee);
        setLoading(false);
        console.log(data);
      } else if (res.data.status === 404) {
        swal({
          title: "Try again!",
          text: "Invalid Id",
          icon: "error",
        });
        history.push(".dfgdfh");
      }
    });
  }, []);
  if (loading) {
    return <img className="loadingedit" src={spinz} alt="fgaaed" />;
  }

  const editemployee = () => {
    
    console.log(id);
    const formData = new FormData();

    formData.append("firstname", firstname);
    formData.append("lastname", lastname);
    formData.append("email", email);
    formData.append("phonenumber", phonenumber);
    formData.append("team", team);
    formData.append("project", project);
    formData.append("file", file);

    console.log(formData);

    axios
      .put(`/api/edit-employee/${id}`, formData)

      .then((res) => {
        console.log(res)
        if (res.status === 200) {
          setError([]);

          swal({
            title: "Good job!",
            text: res.data.message,
            icon: "success",
            button: "Ayoub the best",
          });
        } else if (res.data.status === 422) {
          setError(res.data.error);
          swal({
            title: "Try again!",
            text: "All field are mandetory",
            icon: "error",
          });
        } else if (res.data.status === 404) {
          setError(res.data.error);
          swal({
            title: "Try again!",
            text: res.data.message,
            icon: "error",
          });
        }
      });
  };
  return (
    <div className="addemplyeecontainer">
      <div className="registrationcontainer">
        <div className="registrationbox regtitle">Edit Employee</div>
        <div className="registrationbox">
          <label for="firstname">First Name</label>
          <br />
          <input
            type="text"
            placeholder="Enter your first name"
            name="firstname"
            id="firstname"
            // value={data.first_name}
            onChange={(e) => setFirstname(e.target.value)}
          />
          <small>{error.first_name}</small>
        </div>
        <div className="registrationbox">
          <label for="lastname">Last Name</label>
          <br />
          <input
            type="text"
            placeholder="Enter your last name"
            name="lastname"
            id="lastname"
            // value={data.last_name}
            onChange={(e) => setLastname(e.target.value)}
          />
          <small>{error.last_name}</small>
        </div>
        <div className="registrationbox">
          <label for="email">Email</label>
          <br />
          <input
            type="email"
            placeholder="Enter email"
            name="email"
            id="email"
            // value={data.email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <small>{error.email}</small>
        </div>
        <div className="registrationbox">
          <label for="confirmpassword">Phone Number</label>
          <br />
          <input
            type="text"
            placeholder="Enter phone number"
            name="phonenumber"
            id="phone-number"
            // value={data.phone_number}
            onChange={(e) => setPhonenumber(e.target.value)}
          />
          <small>{error.phone_number}</small>
        </div>
        <select
          className="registrationselect"
          // value={data.team_id}
          name="team"
          onChange={(e) => setTeam(e.target.value)}
        >
          <option value="A">Select Team</option>
          {teamdata.map((vall) => (
            <option value={vall.id}>{vall.name}</option>
          ))}
        </select>
        <small>{error.team_id}</small>
        <select
          className="registrationselect"
          name="project"
          // value={data.project_id}
          onChange={(e) => setProject(e.target.value)}
        >
          <option value="A">Select Project</option>
          {projectdata.map((val) => (
            <option value={val.id}>{val.name}</option>
          ))}
        </select>
        <small>{error.project_id}</small>
        <div className="registrationbox">
          <label for="profile" id="filepath">
            Profile Picture
          </label>

          <br />
          <input
            type="file"
            id="profile"
            name="file"
            // value={data.file_path}
            onChange={(e) => setFile(e.target.files[0])}
          />

          <br />
        </div>
        {/* <div className="registrationbox">
          <label for="filepath">File Path</label>
          <br />
          <input
            type="text"
            placeholder="Enter your file path"
            name="file"
            id="filepath"
            // value={data.file_path}
            onChange={(e) => setFile(e.target.value)}
          />
          <small>{error.file_path}</small>
        </div> */}

        <div className="registrationbox">
          <button type="submit" className="regsub" onClick={editemployee}>
            Update
          </button>
        </div>
      </div>
    </div>
  );
};
