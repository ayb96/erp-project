import React, { useState, useEffect } from "react";
import "./AddEmployee.css";
import { useAmazonContext } from "../../Context/AmazonContext";
import swal from "sweetalert";
import axios from "axios";
export const AddEmployee = () => {
  const { testz, setTestz } = useAmazonContext();
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [phonenumber, setPhonenumber] = useState("");
  const [email, setEmail] = useState("");
  const [team, setTeam] = useState("");
  const [project, setProject] = useState("");
  const [file, setFile] = useState("");
  const [teamdata, setTeamdata] = useState([]);
  const [projectdata, setProjectdata] = useState([]);
  const [error, setError] = useState("");

  useEffect(() => {
    async function fetchData() {
      const req = await axios.get("api/team");
      setTeamdata(req.data);
    }
    async function fetchDataP() {
      const req = await axios.get("api/project");
      setProjectdata(req.data);
      console.log(projectdata);
    }
    fetchDataP();
    fetchData();
    console.log(testz);
  }, []);
  //  axios.get('api/teams').then((res) => {
  //    const result = res.data
  //    setTeamdata(result)
  //   console.log(teamdata)
  //  })

  //  async function searching(key) {
  //   let result = await fetch("http://localhost:8000/api/search/" + key);
  //   result = await result.json();
  //   setData(result);
  // }

  const addemployee = () => {
    const formData = new FormData();
    formData.append("firstname", firstname);
    formData.append("lastname", lastname);
    formData.append("email", email);
    formData.append("phonenumber", phonenumber);
    formData.append("team", team);
    formData.append("project", project);
    formData.append("file", file);
    console.log(formData);

    // const item = {
    //   firstname,
    //   lastname,
    //   email,
    //   phonenumber,
    //   file,
    //   team,
    //   project,
    // };
    console.log(formData);
    axios
      .post("/api/addemployee", formData)

      .then((res) => {
        if (res.data.status === 200) {
          swal({
            title: "Good job!",
            text: "employee Created",
            icon: "success",
            button: "Ayoub the best",
          });
          setError('')
        } else if (res.data.status === 422) {
          console.log(res.data.errors);
          setError(res.data.errors);
        }
      });
  };
  return (
    <div className="addemplyeecontainer">
      <div className="registrationcontainer">
        <div className="registrationbox regtitle">Add Employee</div>
        <div className="registrationbox">
          <label for="firstname">First Name</label>
          <br />
          <input
            type="text"
            placeholder="Enter your first name"
            name="firstname"
            id="firstname"
            value={firstname}
            onChange={(e) => setFirstname(e.target.value)}
          />
          <span>{error.firstname}</span>
        </div>
        <div className="registrationbox">
          <label for="lastname">Last Name</label>
          <br />
          <input
            type="lastname"
            placeholder="Enter your last name"
            name="lastname"
            id="lastname"
            value={lastname}
            onChange={(e) => setLastname(e.target.value)}
          />
          <span>{error.lastname}</span>
        </div>
        <div className="registrationbox">
          <label for="email">Email</label>
          <br />
          <input
            type="email"
            placeholder="Enter email"
            name="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <span>{error.email}</span>
        </div>
        <div className="registrationbox">
          <label for="confirmpassword">Phone Number</label>
          <br />
          <input
            type="text"
            placeholder="Enter phone number"
            name="phonenumber"
            id="phone-number"
            value={phonenumber}
            onChange={(e) => setPhonenumber(e.target.value)}
          />
          <span>{error.phonenumber}</span>
        </div>
        <select
          className="registrationselect"
          value={team}
          onChange={(e) => setTeam(e.target.value)}
          name="team"
        >
          <option value="A">Select Team</option>
          {teamdata.map((vall) => (
            <option value={vall.id}>{vall.name}</option>
          ))}
        </select>
        <span
          style={{
            backgroundColor: "rgb(236, 89, 89)",
            marginLeft: "3px",
            borderRadius: "5px",
            color: "white",
          }}
        >
          {error.team}
        </span>
        <select
          className="registrationselect"
          value={project}
          onChange={(e) => setProject(e.target.value)}
          name="project"
        >
          <option value="A">Select Project</option>
          {projectdata.map((val) => (
            <option value={val.id}>{val.name}</option>
          ))}
        </select>
        <span
          style={{
            backgroundColor: "rgb(236, 89, 89)",
            marginLeft: "3px",
            borderRadius: "5px",
            color: "white",
          }}
        >
          {error.project}
        </span>
        <div className="registrationbox">
          <label for="profile">Profile Picture</label>

          <br />
          <input
            type="file"
            id="filepath"
            name="file"
            onChange={(e) => setFile(e.target.files[0])}
          />
          <span>{error.file}</span>

          <br />
        </div>
        {/* <div className="registrationbox">
          <label for="filepath">File Path</label>
          <br />
          <input
            type="text"
            placeholder="Enter your file path"
            name="filepath"
            id="filepath"
            value={file}
            onChange={(e) => setFile(e.target.value)}
          />
        </div> */}

        <div className="registrationbox">
          <button type="submit" className="regsub" onClick={addemployee}>
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};
