import React from "react";
import "./TestingPage.scss";
export const TestingPage = () => {
  return (
    <div className="skeltontable">
      <table class="tg">
        <tr>
          <th class="tg-cly1">
            <div class="line"></div>
          </th>
          <th class="tg-cly1">
            <div class="line"></div>
          </th>
          <th class="tg-cly1">
            <div class="line"></div>
          </th>
          <th class="tg-cly1">
            <div class="line"></div>
          </th>
          <th class="tg-cly1">
            <div class="line"></div>
          </th>
          <th class="tg-cly1">
            <div class="line"></div>
          </th>
        </tr>
        {[1,3,4].map(() => (
          <tr>
            <td class="tg-cly1">
              <div class="line"></div>
            </td>
            <td class="tg-cly1">
              <div class="line"></div>
            </td>
            <td class="tg-cly1">
              <div class="line"></div>
            </td>
            <td class="tg-cly1">
              <div class="line"></div>
            </td>
            <td class="tg-cly1">
              <div class="line"></div>
            </td>
            <td class="tg-cly1">
              <div class="line"></div>
            </td>
          </tr>
        ))}
      </table>
    </div>
  );
};
