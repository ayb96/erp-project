import React, { useState, useEffect } from "react";
import axios from "axios";
import SearchIcon from "@material-ui/icons/Search";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { useHistory, useParams } from "react-router-dom";
import swal from "sweetalert";

export const TeamEmployee = () => {
  const { id } = useParams();
  const [data, setData] = useState([]);
  const history = useHistory();
  useEffect(() => {
    axios.get(`/api/employeeteam/${id}`).then((res) => {
      if (res.status === 200) {
        console.log(res.data);
        setData(res.data);

        console.log(data);
      } else if (res.status === 404) {
        swal({
          title: "Try again!",
          text: "Invalid Id",
          icon: "error",
        });
        history.push(".dfgdfh");
      }
    });
  }, []);

  //   async function searching(key) {

  //     let result = await fetch("http://localhost:8000/api/list");
  //     result = await result.json();
  //     setData(result);
  //   }
  return (
    <div className="fixmeplz">
      <button className="backbutton" onClick={() => history.push("/teamlist")}>
        <ArrowBackIosIcon />
        <span>Back</span>
      </button>
      <div >
        <div>
          <div className="employeelist-charts">
            <input
              type="email"
              placeholder="Enter your email"
              className="loginemail "
              name="adminemail"
              id="adminemail"
              // onChange={(e) => searching(e.target.value)}
              required
            />
            <SearchIcon className="loginiconzemloyeelist" />
          </div>
          <table>
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name </th>
                <th>Email</th>
                <th>Phone Number</th>
                <th>File_Path</th>
              </tr>
            </thead>
            <tbody>
              {data.map((item, index) => (
                <tr key={index}>
                  <td>{item.first_name}</td>
                  <td>{item.last_name}</td>
                  <td>{item.email}</td>
                  <td>{item.phone_number}</td>
                  <td>
                    <img
                      style={{ width: 100, height: 100 }}
                      src={"http://localhost:8000/" + item.file_path}
                      alt=""
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
};
