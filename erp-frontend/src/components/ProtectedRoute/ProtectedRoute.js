import axios from "axios";
import React, { useEffect, useState } from "react";
import swal from "sweetalert";
import { Redirect, useHistory } from "react-router-dom";
import { Viewemployee } from "../MainEmployeePage/Viewemployee";
import { useAmazonContext } from "../../Context/AmazonContext";
import spin from "../../bower_components/SVG-Loaders/svg-loaders/bars.svg";
export const ProtectedRoute = (props) => {
  const {  setAuthButton, authButton } = useAmazonContext();
  const [authenticated, setAuthenticated] = useState(false);
  const [loading, setLoading] = useState(true);
  const history = useHistory();
  useEffect(() => {
    axios
      .get("/api/checkingAuthenticated")
      .then((res) => {
        if (res.data.status === 200) {
          setAuthenticated(true);
          setAuthButton(true)
        }

        setLoading(false);
      })
      .catch((err) => {
        // swal({
        //   title: "Try again!",
        //   text: "You cannot access",
        //   icon: "error",
        // });
        // history.push("/");
      });

    // return () => {
    //   setAuthenticated(false);
    // };
  });
  axios.interceptors.response.use(
    function (response) {
      return response;
    },
    function (error) {
      if (error.response.status === 403) {
        swal({
          title: "Try again!",
          text: error.response.data.message,
          icon: "warning",
        });
        history.push("/");
      } else if (error.response.status === 404) {
        swal({
          title: "Try again!",
          text: error.response.data.message,
          icon: "warning",
        });
        history.push("/ssss");
      } else if (error.response.status === 401) {
        swal({
          title: "Try again!",
          text: error.response.data.message,
          icon: "warning",
        });
        history.push("/");
      }
      return Promise.reject(error);
    }
  );

  if (loading) {
    return <img src={spin} alt="fgaaed" />;
  }

  let Cmp = props.Cmp;

  return <div>{authenticated ? <Cmp /> : <Viewemployee />}</div>;
};
